<?php 
/*
Plugin Name: Ramble Theme Shortcodes
Plugin URI: http://www.ramble.softhopper.net
Description: This plugin will include Ramble theme shortcode
Author: SoftHopper
Author URI: http://softhopper.net
Version: 1.0
*/

/* don't call the file directly */
if ( !defined( 'ABSPATH' ) ) exit;

// include shortcode file to generate shortcode from code editor
include( plugin_dir_path( __FILE__ ) . 'shortcodes/shortcodes.php' );

// Support shortcodes in text widgets
add_filter( 'widget_text', 'do_shortcode' );