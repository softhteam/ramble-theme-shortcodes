<?php
/*
	Ramble Shortcode Module for Ramble themes
	Version:  1.0.0
*/

if ( is_admin() ) {
	/*-----------------------------------------------------------------------------------*/
	# Register main Scripts and Styles
	/*-----------------------------------------------------------------------------------*/
	function ramble_admin_register() {
		wp_enqueue_script( 'shortcodes-js', plugins_url( '/js/shortcodes.js', __FILE__ ), array('jquery'), 1.0, false); 

	    $categories = get_categories();
		$category_list = array();  
	    foreach ($categories as $category) {
	        if ( $category->slug == 'uncategorized') continue;
	        $category_list[] = array("text" => $category->cat_name, "value" => $category->slug);
	    }

		$ramble_lang = array( 				
			"shortcode_ramble"		=> esc_html__( 'Ramble Shortcodes', 'ramble' ), 
			"shortcode_category_list"	=> $category_list, 
			"shortcode_magazine"		=> esc_html__( 'Magazine', 'ramble' ), 
			"shortcode_cat_block_style_col"		=> esc_html__( 'Category Block Style Column', 'ramble' ), 
			"shortcode_cat"				=> esc_html__( 'Category', 'ramble' ), 
			"shortcode_number_of_posts"	=> esc_html__( 'Number of posts', 'ramble' ), 
			"shortcode_1st_post_exc_length"	=> esc_html__( '1st post excerpt length', 'ramble' ), 
			"shortcode_show_item"		=> esc_html__( 'Show item', 'ramble' ), 
			"shortcode_cat_block_style_tab_1"	=> esc_html__( 'Category Block Style Tab One', 'ramble' ), 
			"shortcode_cat_block_style_tab_2"	=> esc_html__( 'Category Block Style Tab Two', 'ramble' ), 
			"shortcode_cat_block_style_tab_3"	=> esc_html__( 'Category Block Style Tab Three', 'ramble' ), 
			"shortcode_cat_block_style_slider"	=> esc_html__( 'Category Block Style Slider', 'ramble' ), 
			"shortcode_cat_slug"		=> esc_html__( 'Category Slug', 'ramble' ), 
			"shortcode_cat_slug_description"		=> esc_html__( "To get category slug go to <a style='color:green; font-weight:bold;' target='_blank' href='edit-tags.php?taxonomy=category'>here.</a> To add multiple category <br> add with comma like this => cat_slug1, cat_slug2<br><span style='font-weight:bold;'>Note :</span> If you don't give any slug it will show all latest posts", 'ramble' ), 
			"shortcode_cat_column"		=> esc_html__( 'Category Column', 'ramble' ), 
			"shortcode_cat_column_size"		=> esc_html__( 'Category Column Size', 'ramble' ), 
			"shortcode_tab_title"		=> esc_html__( 'Tab Title', 'ramble' ), 
			"shortcode_tab_position"	=> esc_html__( 'Tab position', 'ramble' ), 
			"shortcode_img_position"	=> esc_html__( 'Image position', 'ramble' ), 
			"shortcode_big_post_position"	=> esc_html__( 'Big post position', 'ramble' ), 
			"shortcode_post_layout_style"	=> esc_html__( 'Post layout style', 'ramble' ), 
			"shortcode_grid"			=> esc_html__( 'Grid', 'ramble' ), 
			"shortcode_6_grid"			=> esc_html__( '6 Grid', 'ramble' ), 
			"shortcode_4_grid"			=> esc_html__( '4 Grid', 'ramble' ), 
			"shortcode_3_grid"			=> esc_html__( '3 Grid', 'ramble' ), 
			"shortcode_masonry"			=> esc_html__( 'Masonry', 'ramble' ), 
			"shortcode_right"			=> esc_html__( 'Right', 'ramble' ), 
			"shortcode_left"			=> esc_html__( 'Left', 'ramble' ), 
			"shortcode_top"				=> esc_html__( 'Top', 'ramble' ), 
			"shortcode_bottom"			=> esc_html__( 'Bottom', 'ramble' ), 
			"shortcode_center"			=> esc_html__( 'Center', 'ramble' ), 
			"shortcode_width"			=> esc_html__( 'Width', 'ramble' ), 
			"shortcode_content"			=> esc_html__( 'Content', 'ramble' ), 
			"shortcode_button"			=> esc_html__( 'Button', 'ramble' ), 
			"shortcode_type"			=> esc_html__( 'Type', 'ramble' ), 
			"shortcode_default"			=> esc_html__( 'Default', 'ramble' ), 
			"shortcode_primary"			=> esc_html__( 'Primary', 'ramble' ), 
			"shortcode_success"			=> esc_html__( 'Success', 'ramble' ), 
			"shortcode_info"			=> esc_html__( 'Info', 'ramble' ), 
			"shortcode_warning"			=> esc_html__( 'Warning', 'ramble' ), 
			"shortcode_danger"			=> esc_html__( 'Danger', 'ramble' ),
			"shortcode_size"			=> esc_html__( 'Size', 'ramble' ),
			"shortcode_size_large"		=> esc_html__( 'Large', 'ramble' ), 
			"shortcode_size_default" 	=> esc_html__( 'Default', 'ramble' ), 
			"shortcode_size_small"		=> esc_html__( 'Small', 'ramble' ), 
			"shortcode_size_ex_small"	=> esc_html__( 'Extra Small', 'ramble' ), 
			"shortcode_progressbar"		=> esc_html__( 'Progress Bar', 'ramble' ), 
			"shortcode_link"			=> esc_html__( 'Link', 'ramble' ), 
			"shortcode_text"			=> esc_html__( 'Text', 'ramble' ), 
			"shortcode_title"			=> esc_html__( 'Title', 'ramble' ), 
			"shortcode_state"			=> esc_html__( 'State', 'ramble' ), 
			"shortcode_status"			=> esc_html__( 'Status', 'ramble' ), 
			"shortcode_url"				=> esc_html__( 'URL', 'ramble' ), 
			"shortcode_background_img"	=> esc_html__( 'Background Image', 'ramble' ), 
			"shortcode_opened"			=> esc_html__( 'Opened', 'ramble' ), 
			"shortcode_closed"			=> esc_html__( 'Closed', 'ramble' ), 
			"shortcode_true"			=> esc_html__( 'True', 'ramble' ), 
			"shortcode_false"			=> esc_html__( 'False', 'ramble' ), 
			"shortcode_alert"			=> esc_html__( 'Alert', 'ramble' ), 
			"shortcode_dismiss"			=> esc_html__( 'Dismiss', 'ramble' ), 
			"shortcode_striped"			=> esc_html__( 'Striped', 'ramble' ), 
			"shortcode_animation"		=> esc_html__( 'Animation', 'ramble' ), 
			"shortcode_height"			=> esc_html__( 'Height:', 'ramble' ), 
			"shortcode_video"			=> esc_html__( 'Video', 'ramble' ), 
			"shortcode_video_url"		=> esc_html__( 'Video URL:', 'ramble' ), 
			"shortcode_audio"			=> esc_html__( 'Audio', 'ramble' ), 
			"shortcode_mp3"				=> esc_html__( 'MP3 file URL', 'ramble' ), 
			"shortcode_m4a"				=> esc_html__( 'M4A file URL', 'ramble' ), 
			"shortcode_ogg"				=> esc_html__( 'OGG file URL', 'ramble' ), 
			"shortcode_tooltip"			=> esc_html__( 'ToolTip', 'ramble' ), 
			"shortcode_direction"		=> esc_html__( 'Direction', 'ramble' ), 
			"shortcode_facebook"		=> esc_html__( 'Facebook', 'ramble' ), 
			"shortcode_tweeter"			=> esc_html__( 'Tweeter', 'ramble' ), 
			"shortcode_google_plus"		=> esc_html__( 'Google Plus', 'ramble' ),
			"shortcode_dropcap"			=> esc_html__( 'Dropcap', 'ramble' ),
			"shortcode_columns"			=> esc_html__( 'Columns', 'ramble' ),
			"shortcode_accordion"		=> esc_html__( 'Accordion', 'ramble' ),
			"shortcode_tab"				=> esc_html__( 'Tab', 'ramble' ),
			"shortcode_custom_icon"		=> esc_html__( 'Insert Custom Icon', 'ramble' ),
			);		
		wp_localize_script( 'shortcodes-js', 'rambleLang', $ramble_lang );	
	}
	add_action( 'admin_enqueue_scripts', 'ramble_admin_register' ); 
} // end is_admin()
 
add_action('admin_head', 'ramble_add_mce_button');
function ramble_add_mce_button() {
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return;
	}
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_external_plugins', 'ramble_add_tinymce_plugin' );
		add_filter( 'mce_buttons', 'ramble_register_mce_button' );
	}
}

// Declare script for new button
function ramble_add_tinymce_plugin( $plugin_array ) {
	$plugin_array['ramble_mce_button'] = plugins_url( '/mce.js', __FILE__ );
	return $plugin_array;
}

// Register new button in the editor
function ramble_register_mce_button( $buttons ) {
	array_push( $buttons, 'ramble_mce_button' );
	return $buttons;
}


// status shortcode for facebook and twitter
add_shortcode("status", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => '',
			'url' => '',
			'background' => '',
		), $atts
	);
	extract($atts);

	( !empty($background) ) ? $background = $background : $background = "";
	$status_facebook = ( $type == "facebook" ) ? "facebook" : "";
	$status_twitter = ( $type == "twitter" ) ? "twitter" : "";
	$status_gplus = ( $type == "google_plus" ) ? "google_plus" : "";

	if( !empty( $status_facebook ) || !empty( $status_twitter ) || !empty( $status_gplus ) ):	?>
		<div class="post-status-wrapper" style="background: url(<?php echo $background; ?>);">
		<?php if( !empty( $status_facebook ) ) : ?>
			<div id="fb-root"></div>
			<script>
				(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;  js = d.createElement(s);
				js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
				fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-post" data-href="<?php echo $url ?>"></div>
		<?php elseif( !empty( $status_twitter ) ) : ?>
			<blockquote class="twitter-tweet"><a href="<?php echo $url ?>"></a></blockquote>
			<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
		<?php elseif( !empty( $status_gplus ) ) : ?>
			<script type="text/javascript" src="//apis.google.com/js/plusone.js"></script>
			<div class="g-post" data-href="<?php echo $url ?>"></div>
		<?php endif; ?>
		</div><!-- /.post-status-wrapper -->
	<?php endif; 
	$status = ob_get_clean();
	return $status;
});

// bootstrap column grid row shortcode
add_shortcode("row", function($atts, $content = null) {
	return '<div class="row">' . do_shortcode( $content ) . '</div>';
});

// bootstrap column grid shortcode
add_shortcode("column", function($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'number' => '',
			'offset' => '',
		), $atts
	);
	extract($atts);
	if ( !empty($offset) ) {
		$offset = "col-md-offset-$offset";
	}
	return "<div class='$offset col-md-$number'>". do_shortcode( $content ) ."</div>";
});

// bootstrap progress bar shortcode 
add_shortcode("progress_bar", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => '',
			'width' => '50',
			'title' => '',
			'striped' => false,
			'animation' => false,
		), $atts
	);
	extract($atts);

	( !empty($type) ) ? $type = " progress-bar-$type " : $type = "";
	( $striped == true ) ? $striped = " progress-bar-striped " : $striped = "";
	( $animation == true ) ? $animation = " active " : $animation = "";
	?>

	<div class="progress">
		<div class="progress-bar <?php echo $type.$striped.$animation;?>" role="progressbar" aria-valuenow="<?php echo $width; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $width; ?>%">
	    <?php echo $title; ?>
	  	</div> <!-- /.progress-bar -->
	</div> <!-- /.progress -->

	<?php	
	$progress_bar = ob_get_clean();
	return $progress_bar;
});

// bootstrap button shortcode 
add_shortcode("button", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => 'primary',
			'text' => '',
			'size' => '',
			'disabled' => false,
		), $atts
	);
	extract($atts);

	( !empty($type) ) ? $type = " btn-$type " : $type = "";
	( $disabled == true ) ? $disabled = " disabled='true' " : $disabled = "";
	( $text == null ) ? $text = __(" Button ", "ramble") : $text = $text;
	?>

	<button type="button" class="btn <?php echo $type." ".$size; ?>" <?php echo $disabled; ?>>
	<?php echo $text; ?>
	</button>

	<?php	
	$button = ob_get_clean();
	return $button;
});


// bootstrap accordion shortcode collapse_group
add_shortcode("collapse_group", function($atts, $content = null) {
	return '<div class="panel-group" id="accordion" role="tablist" aria-multiselecategoryTable="true">' . do_shortcode( $content ) . '</div>';
});

// bootstrap accordion shortcode collapse 
add_shortcode("collapse", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'id' => '',
			'title' => '',
			'expanded' => "false",
		), $atts
	);
	extract($atts);
	( $expanded == "true" ) ? $aria_expander = "true" : $aria_expander = "false";
	( $expanded == "true" ) ? $expanded_two = "" : $expanded_two = "collapsed";
	( $expanded == "true" ) ? $expanded_in = "in" : $expanded_in = "";
	?>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab">
		  <h4 class="panel-title">
		    <a class="accordion-toggle <?php echo $expanded_two; ?>" role="button" data-toggle="collapse" href="#<?php  echo $id; ?>" aria-expanded="<?php echo $aria_expander; ?>" >
		    <?php  echo $title; ?>
		    </a>
		  </h4>
		</div>
		<div id="<?php echo $id; ?>" class="panel-collapse collapse <?php echo $expanded_in; ?>" role="tabpanel"  aria-expanded="<?php echo $aria_expander; ?>">
		  <div class="panel-body">
		  <?php  echo $content; ?>
		  </div>
		</div>
	</div>
	<?php	
	$collapse = ob_get_clean();
	return $collapse;
});

// bootstrap shortcode alert 
add_shortcode("alert", function($atts, $content = null) {
	ob_start();	
	$atts = shortcode_atts(
		array(
			'type' => '',
			'dismiss' => false,
		), $atts
	);
	extract($atts);
	?>
	<div class="alert alert-<?php echo $type;?> " role="alert">
	<?php
	  if ( $dismiss == true ) {
	  	?>
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  	<?php
	  }
	  echo $content;
	?>
	</div><!-- /.alert -->
	<?php	
	$alert = ob_get_clean();
	return $alert;
});

// bootstrap shortcode tooltip 
add_shortcode("tooltip", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'text' => '',
			'direction' => "top",
		), $atts
	);
	extract($atts);
	?>
	<span data-toggle="tooltip" title="<?php echo $text; ?>" data-placement="<?php echo $direction; ?>"><?php echo $content; ?></span>

	<?php	
	$tooltip = ob_get_clean();
	return $tooltip;
});

// custom icon font
add_shortcode("custom_icon", function($atts, $content = null) {
	ob_start();	
	$atts = shortcode_atts(
		array(
			'name' => '',
			'class' => '',
			'font_size' => '',
			'color' => '',
		), $atts
	);
	extract($atts); ?>
	<i class="<?php echo esc_attr($name); ?> <?php echo esc_attr($class); ?>" style="font-size:<?php echo esc_attr($font_size); ?>; color: <?php echo esc_attr($color); ?>;"></i>
	<?php	
	$custom_icon = ob_get_clean();
	return $custom_icon;
});

// bootstrap shortcode dropcap 
add_shortcode("dropcap", function($atts, $content = null) {
	ob_start();	
	?>
	<span class="dropcap"><?php echo $content; ?></span>
	<?php	
	$dropcap = ob_get_clean();
	return $dropcap;
});

// bootstrap tabs shortcode 
$tabs_divs = '';

function tabs_group($atts, $content = null ) {
    global $tabs_divs;
    $tabs_divs = '';
    $output = '<div id="tab-side-container"><ul class="nav nav-tabs"';
    $output.='>'.do_shortcode($content).'</ul>';
    $output.= '<div class="tab-content">'.$tabs_divs.'</div></div>';
    return $output;  
}  


function tab($atts, $content = null) {  
    global $tabs_divs;
    extract(shortcode_atts(array(  
        'id' => '',
        'title' => '', 
        'active' => '', 
    ), $atts));  
    if(empty($id))
        $id = 'side-tab'.rand(100,999);
    $output = '
        <li class="'.$active.'">
            <a href="#'.$id.'" data-toggle="tab">'.$title.'</a>
        </li>
    ';
    $tabs_divs.= '<div class="tab-pane '.$active.'" id="'.$id.'">'.$content.'</div>';
    return $output;
}

add_shortcode('tabs', 'tabs_group');
add_shortcode('tab', 'tab');

/* Magazine Shortcode */
function ramble_theme_mag_excerpt_length($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);

	if (count($excerpt)>=$limit) {

	array_pop($excerpt);

	$excerpt = implode(" ",$excerpt).'...';

	} else {

	$excerpt = implode(" ",$excerpt);

	}

	$excerpt = preg_replace('`[[^]]*]`','',$excerpt);

	return $excerpt;
}
// category_block_style_column
add_shortcode("category_block_style_column", function($atts, $content = null) {
	STATIC $cat_column_shortcode_id = 1;
	ob_start();	
	$atts = shortcode_atts(
		array(
			'category_slug' => '',
			'total_post' => 5,
			'excerpt_length' => 350,
			'category_column' => 'col-md-6',
		), $atts
	);
	extract($atts);
	?>
	<div class="category-style-two <?php echo $category_column; ?> col-sm-6">                                
	    <div class="category-content">
            	<?php 
            	global $wp_query;
            	$cat_slug = get_category_by_slug($category_slug); 
            	$cat_id = $cat_slug->term_id;
            	$wp_query = new WP_Query(
            	    array (
            	        'category_name' => $category_slug,
            	        'ignore_sticky_posts' => true,
            	    )
            	);
            	
            	if ( $wp_query->have_posts() ) : ?>
            	<?php 
            	$count = 0;
            	while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
 				<?php if (++$count > 1) break; ?>
            	
                <div class="category-heading">
                    <div class="left-section">
                        <h4><a href="<?php echo esc_url( get_category_link( $cat_id ) ); ?>"><?php esc_html_e( $cat_slug->name ); ?></a></h4>
                    </div> <!-- /.left-section -->
                </div> <!-- /.category-heading -->

                <div class="post post-normal">
                    <figure class="post-thumb">
                        <?php
                       		ramble_theme_get_featured_img_with_link('ramble-theme-portfolio-image');
                        ?>
                    </figure> <!-- /.post-thumb --> 

                    <header class="entry-header">
                        <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                        <div class="entry-meta">
                            <div class="meta-content">            
                                <div class="meta">          
                                    <span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
                                    <span class="devider">/</span>
                                    <span class="byline">
                                        <span class="author vcard">
                                            <?php esc_html_e('By: ','ramble').the_author_posts_link(); ?>
                                        </span>
                                    </span>
                                    <span class="devider">/</span>
                                    <span  class="comments-link"><a href="<?php comments_link(); ?>">
                                        <?php comments_number( esc_html__( 'No Comments', 'ramble' ), esc_html__( '1 Comment', 'ramble' ), '%'.esc_html__( 'Comments', 'ramble' ) ); ?>
                                    </a></span>
                                </div> <!-- /.meta -->
                            </div>
                        </div> <!-- .entry-meta -->
                    </header> <!-- /.entry-header -->
                    <div class="entry-content">
                        <span class="screen-reader-text"></span>
                        <p><?php echo ramble_theme_mag_excerpt_length($excerpt_length); ?></p>
                    </div> <!-- .entry-content -->
                </div> <!-- /.post -->

            	<?php endwhile; ?>
                <?php endif; ?>

                <ul class="post-small">     
                   	<?php 
                   	$total_posts = $total_post - 1;
                   	$wp_query = new WP_Query(
                   	    array (
                   	        'category_name' => $category_slug,
                   	        'posts_per_page' => $total_posts,
                   	        'offset' => 1,
                   	        'ignore_sticky_posts' => true,
                   	    )
                   	);
                   	
                   	if ( $wp_query->have_posts() ) : ?>
                   	<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
                    <li class="post">
                        <div class="item-text">
                            <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                            <div class="entry-meta">        
                                <span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
                                <span class="devider">/</span>
                                <span  class="comments-link"><a href="<?php comments_link(); ?>">
                                    <?php comments_number( esc_html__( 'No Comments', 'ramble' ), esc_html__( '1 Comment', 'ramble' ), '%'.esc_html__( 'Comments', 'ramble' ) ); ?>
                                </a></span>
                            </div>
                        </div> <!-- /.item-text -->
                    </li> <!-- /.post -->
                	<?php endwhile; ?>
                    <?php endif; ?>                   
                    
                </ul> <!-- /.post-small -->  
	    </div> <!-- /.category-content -->
	</div> <!-- /#category-style-two -->
	<?php	
	$cat_column_shortcode_id++;
	$category_block_style_column = ob_get_clean();
	return $category_block_style_column;
}); // end category_block_style_column

// category_block_style_slider
add_shortcode("category_block_style_slider", function($atts, $content = null) {
	STATIC $slider_shortcode_id = 1;
	ob_start();	
	$atts = shortcode_atts(
		array(
			'category_slug' => '',
			'total_post' => 5,
			'excerpt_length' => 350,
			'show_item' => '2',
		), $atts
	);
	extract($atts);

	global $wp_query;
	$cat_slug = get_category_by_slug($category_slug); 
	$cat_id = $cat_slug->term_id;
	?>
	<div class="category-style-three col-md-12">                                
        <div class="category-heading">
            <div class="left-section">
                <h4><a href="<?php echo esc_url( get_category_link( $cat_id ) ); ?>"><?php esc_html_e( $cat_slug->name ); ?></a></h4>     
            </div> <!-- /.left-section -->
            <div class="right-section">
                <div class="control-video-carousel category-slider-id-<?php echo $slider_shortcode_id; ?>">
                    <span class="previous">
                        <i class="fa fa-angle-left"></i>
                    </span>
                    <span class="next">
                        <i class="fa fa-angle-right"></i>
                    </span>
                </div>
            </div> <!-- /.right-section -->
        </div> <!-- /.category-heading -->
        <div class="category-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="category-slider-<?php echo $slider_shortcode_id; ?> owl-carousel">
                    	<?php 
                    	$wp_query = new WP_Query(
                    	    array (
                    	        'category_name' => $category_slug,
                    	        'posts_per_page' => $total_post,
                    	        'ignore_sticky_posts' => true,
                    	    )
                    	);
                    	
                    	if ( $wp_query->have_posts() ) : ?>
                    	<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
                        <div class="item">
                            <div class="post-thumb">
                                <?php
                               		ramble_theme_get_featured_img_with_link('ramble-theme-blog-two-grid');
                                ?>
                            </div>
                            <div class="post-content">
                                <div class="entry-header">
                                    <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                                    <div class="entry-meta"> 
                                        <span class="cat-links">
                                            <?php esc_html_e('In: ','ramble').the_category(' '); ?>
                                        </span>       
                                        <span class="devider">/</span>
                                        <span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
                                        <span class="devider">/</span>
                                        <span class="byline">
                                            <span class="author vcard">
                                                <?php esc_html_e('By: ','ramble').the_author_posts_link(); ?>
                                            </span>
                                        </span>
                                    </div>  
                                </div> <!-- /.entry-header -->
                                <div class="entry-content">
                                    <span class="screen-reader-text"></span>
                                    <p><?php echo ramble_theme_mag_excerpt_length($excerpt_length); ?></p>
                                </div> <!-- .entry-content -->
                            </div>   
                        </div> <!-- /.item -->
                    	<?php endwhile; ?>
                        <?php endif; ?>
                        
                    </div> <!-- /#category-carousel -->       
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.category-content -->
    </div> <!-- /#category-style-three -->
    <script>
    	(function($) {
    		$(function() {
		    	var carousel_category = $('.category-slider-<?php echo $slider_shortcode_id; ?>');           
		    	carousel_category.owlCarousel({
		    	    items: <?php echo $show_item; ?>,
		    	    singleItem: false, 
		    	    margin: 30,
		    	    responsive: {
		    	        280: {
		    	            items: 1
		    	        },
		    	        500: {
		    	            items: 1
		    	        },
		    	        600: {
		    	            items: 1
		    	        },
		    	        700: {
		    	            items: 2
		    	        },
		    	        768: {
		    	            items: <?php echo $show_item; ?>
		    	        }
		    	    }
		    	});
		    	$('.category-slider-id-<?php echo $slider_shortcode_id; ?> .next').on('click',function() {
		    	    carousel_category.trigger('next.owl.carousel');
		    	});
		    	$('.category-slider-id-<?php echo $slider_shortcode_id; ?> .previous').on('click',function() {
		    	    // With optional speed parameter
		    	    // Parameters has to be in square bracket '[]'
		    	    carousel_category.trigger('prev.owl.carousel', [300]);
		    	});
	   
	        });
	    })(jQuery);
    </script>
	<?php	
	$slider_shortcode_id++;
	$category_block_style_slider = ob_get_clean();
	return $category_block_style_slider;
}); // end category_block_style_slider

// category_block_style_tab_one
add_shortcode("category_block_style_tab_one", function($atts, $content = null) {
	STATIC $tab_shortcode_id = 1;
	ob_start();	
	$atts = shortcode_atts(
		array(
			'category_slug' => '',
			'tab_title' => '',
			'total_post' => 5,
			'excerpt_length' => 350,
			'tab_position' => 'right',
			'big_post_position' => 'left',
		), $atts
	);
	extract($atts);
	?>

	<div id="category-tab-<?php echo $tab_shortcode_id; ?>" class="<?php echo ($big_post_position == 'right') ? 'category-style-five' : 'category-style-one'; ?> col-md-12">                                
        <div class="category-heading">
            <div class="left-section <?php if ($tab_position == 'left') echo 'pull-right'; ?>">
            	<?php
            	if ( !empty($tab_title) ) { 
            		echo "<h4>$tab_title</h4>";
            	} else {
            		echo "<h4>".esc_html__('Latest Post', 'ramble')."</h4>";
            	}
            	?> 
            </div> <!-- /.left-section -->
            <div class="right-section <?php if ($tab_position == 'left') echo 'pull-left'; ?>">
                <ul>
                	<?php
                		$category_slug = str_replace(" ","", $category_slug);
                		$category_slugs = explode(",", $category_slug);
                		$tab_li_i = 1;
                		if ( !empty($category_slug) ) {
	                	echo "<li class='current'><a href='#$tab_shortcode_id-tab-0'>".__('All', 'ramble')."</a></li>";
		                }
                		foreach ($category_slugs as $key => $value) {
                			if ( !empty($value) ) {
                				$cat_name = get_category_by_slug($value); 
	                			echo "<li><a href='#$tab_shortcode_id-tab-$tab_li_i'>".$cat_name->name."</a></li>";
                				$tab_li_i++;
                			}
                		}
                	?>	
                </ul>
            </div> <!-- /.right-section -->
        </div> <!-- /.category-heading -->
        <div class="tab">
        	<?php 
    		for ($x = 0; $x < $tab_li_i; $x++) {
    			$cat_slugs = array_filter($category_slugs);
    			if ($x == 0) {
	    			$cat_slug = $cat_slugs;
    			} else {
    				$cat_slug = $cat_slugs[$x-1];
    			}
			?>
            <div id="<?php echo $tab_shortcode_id; ?>-tab-<?php echo $x; ?>" class="tab-container">                                            
                <div class="category-content">
                    <div class="row">
                        <div class="col-md-7 col-sm-7 <?php if ($big_post_position == 'right') echo 'col-md-push-5 col-sm-push-5'; ?>">
            				<?php 
		        	    	if ( !empty($category_slug) ) {
			        	    	$post_by_cat_condition = array(
		        		    	        array(
		        		    	            'taxonomy' => 'category',
		        		    	            'field' => 'slug',
		        		    	            'terms' => $cat_slug 
		        		    	        )
		        		    	    );
		        	    	} else {
		        	    		$post_by_cat_condition = '';
		        	    	}
            				$wp_query = new WP_Query(
            				    array (
            				    	'tax_query' => $post_by_cat_condition,
            				        'ignore_sticky_posts' => true,
            				    )
            				);
            				if ( $wp_query->have_posts() ) : ?>
            				<?php 
            	        	$count = 0;
            	        	while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
            				<?php if (++$count > 1) break; ?>
                            <div class="post post-normal">
                                <figure class="post-thumb">
                                    <?php
                                   		ramble_theme_get_featured_img_with_link('ramble-theme-portfolio-image');
                                    ?>
                                </figure> <!-- /.post-thumb --> 

                                <header class="entry-header">
                                    <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                                    <div class="entry-meta">
                                        <div class="meta-content">            
                                            <div class="meta">          
                                                <span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
                                                <span class="devider">/</span>
                                                <span class="byline">
                                                    <span class="author vcard">
                                                        <?php esc_html_e('By: ','ramble').the_author_posts_link(); ?>
                                                    </span>
                                                </span>
                                                <span class="devider">/</span>
                                                <span  class="comments-link"><a href="<?php comments_link(); ?>">
			                                        <?php comments_number( esc_html__( 'No Comments', 'ramble' ), esc_html__( '1 Comment', 'ramble' ), '%'.esc_html__( 'Comments', 'ramble' ) ); ?>
			                                    </a></span>
                                            </div> <!-- /.meta -->
                                        </div>
                                    </div> <!-- .entry-meta -->
                                </header> <!-- /.entry-header -->
                                <div class="entry-content">
                                    <span class="screen-reader-text"></span>
                                    <p><?php echo ramble_theme_mag_excerpt_length($excerpt_length); ?></p>
                                </div> <!-- .entry-content -->
                            </div> <!-- /.post -->
                        	<?php endwhile; ?>
                            <?php endif; ?>
                        </div> <!-- /.col-md-6 -->  
                        <div class="col-md-5 col-sm-5 <?php if ($big_post_position == 'right') echo 'col-md-pull-7 col-sm-pull-7'; ?>">
                            <ul class="post-small">  
                            	<?php 
                            	$total_posts = $total_post - 1;
                            	$wp_query = new WP_Query(
                            	    array (
                            	    	'tax_query' => $post_by_cat_condition,
                            	        'posts_per_page' => $total_posts,
                            	        'offset' => 1,
                            	        'ignore_sticky_posts' => true,
                            	    )
                            	);
                            	
                            	if ( $wp_query->have_posts() ) : ?>
                            	<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
                                <li class="post hentry">
                                    <div class="image-area">
                                            <div class="post-thumb">
		                                        <?php
                                               		ramble_theme_get_featured_img_with_link('ramble-theme-medium-img');
                                                ?>
                                            </div>
                                    </div> <!-- /.image-area -->
                                    <div class="item-text">
                                        <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                                        <div class="entry-meta">        
                                            <span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
                                            <span class="devider">/</span>
                                            <span  class="comments-link"><a href="<?php comments_link(); ?>">
	                                        <?php comments_number( esc_html__( 'No Comments', 'ramble' ), esc_html__( '1 Comment', 'ramble' ), '%'.esc_html__( 'Comments', 'ramble' ) ); ?>
		                                    </a>
		                                    </span>
                                        </div>
                                    </div> <!-- /.item-text -->
                                </li> <!-- /.post -->
                                <?php endwhile; ?>
	                            <?php endif; ?>
                               
                            </ul> <!-- /.post-small -->   
                        </div> <!-- /.col-md-6 -->  
                    </div> <!-- /.row -->
                </div> <!-- /.category-content -->
            </div> <!-- /.tab-content -->
			<?php } ?>
        </div> <!-- /.tab -->
    </div> <!-- /#category-style-one -->
    <script>
    	(function($) {
    		"use strict";
    		var content_height_left = $(".category-content > .row > .col-md-7").outerHeight(true),
    		    content_height_right = $(".category-content > .row > .col-md-5").outerHeight(true),
    		    max_height = Math.max(content_height_left, content_height_right);
    		$('<style>@media only screen and (min-width: 992px) {#content-magazine .category-content > .row > .col-md-5 > .post-small{min-height:'+ parseInt(max_height - 10)+'px;}}</style>').appendTo('head');    		

	    	$("#category-tab-<?php echo $tab_shortcode_id; ?> .category-heading .right-section").each(function() {
        	    $(this).find('li:first').addClass("current");
        	    $(this).parents().next('.tab').find('.tab-container:first').show();
        	});
        	$("#category-tab-<?php echo $tab_shortcode_id; ?> .category-heading .right-section li a").on('click',function() {
        	    var categoryTab = $(this).closest('li');
        	    categoryTab.siblings('li').removeClass("current");
        	    categoryTab.addClass("current");
        	    categoryTab.closest('#category-tab-<?php echo $tab_shortcode_id; ?> .category-heading .right-section').parents().nextAll('.tab:first').find('.tab-container').hide(); 

        	    var activeTab = $(this).attr("href"); 
        	    $(activeTab).fadeIn(); 
        	    return false;
        	});
	    })(jQuery);
    </script>
	<?php	
	$tab_shortcode_id++;
	$category_block_style_tab_one = ob_get_clean();
	return $category_block_style_tab_one;
}); // end category_block_style_tab_one

// category_block_style_tab_two
add_shortcode("category_block_style_tab_two", function($atts, $content = null) {
	STATIC $tab_shortcode_id = 5;
	ob_start();	
	$atts = shortcode_atts(
		array(
			'category_slug' => '',
			'tab_title' => '',
			'total_post' => 5,
			'excerpt_length' => 350,
			'tab_position' => 'right',
			'img_position' => 'left',
		), $atts
	);
	extract($atts);
	?>

	<div id="category-tab-<?php echo $tab_shortcode_id; ?>" class="all-latest-post list col-md-12">                                
        <div class="category-heading">
            <div class="left-section <?php if ($tab_position == 'left') echo 'pull-right'; ?>">
            	<?php
            	if ( !empty($tab_title) ) { 
            		echo "<h4>$tab_title</h4>";
            	} else {
            		echo "<h4>".esc_html__('Latest Post', 'ramble')."</h4>";
            	}
            	?> 
            </div> <!-- /.left-section -->
            <div class="right-section <?php if ($tab_position == 'left') echo 'pull-left'; ?>">
                <ul>
                	<?php
                		$category_slug = str_replace(" ","", $category_slug);
                		$category_slugs = explode(",", $category_slug);
                		$tab_li_i = 1;
                		if ( !empty($category_slug) ) {
		                	echo "<li class='current'><a href='#$tab_shortcode_id-tab-0'>".__('All', 'ramble')."</a></li>";
		                }
                		foreach ($category_slugs as $key => $value) {
                			if ( !empty($value) ) {
                				$cat_name = get_category_by_slug($value); 
	                			echo "<li><a href='#$tab_shortcode_id-tab-$tab_li_i'>".$cat_name->name."</a></li>";
                				$tab_li_i++;
                			}
                		}
                	?>	
                </ul>
            </div> <!-- /.right-section -->
        </div> <!-- /.category-heading -->
        <div class="tab">
        	<?php 
    		for ($x = 0; $x < $tab_li_i; $x++) {
    			$cat_slugs = array_filter($category_slugs);
    			if ($x == 0) {
	    			$cat_slug = $cat_slugs;
    			} else {
    				$cat_slug = $cat_slugs[$x-1];
    			}
			?>
            <div id="<?php echo $tab_shortcode_id; ?>-tab-<?php echo $x; ?>" class="tab-container">                                            
                <div class="category-content">
        	    	<?php 
        	    	$total_posts = $total_post;
        	    	if ( !empty($category_slug) ) {
	        	    	$post_by_cat_condition = array(
        		    	        array(
        		    	            'taxonomy' => 'category',
        		    	            'field' => 'slug',
        		    	            'terms' => $cat_slug 
        		    	        )
        		    	    );
        	    	} else {
        	    		$post_by_cat_condition = '';
        	    	}
        	    	$wp_query = new WP_Query(
        	    	    array (
        	    	    	'tax_query' => $post_by_cat_condition,
        	    	        'posts_per_page' => $total_posts,
        	    	        'ignore_sticky_posts' => true,
        	    	    )
        	    	);
        	    	
        	    	if ( $wp_query->have_posts() ) : ?>
        	    	<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
                    <div class="post">
                        <div class="row">
                            <div class="col-md-5 col-sm-5 <?php if ($img_position == 'right') echo 'col-md-push-7 col-sm-push-7'; ?>">
                                <figure class="post-thumb">
                                    <?php
                                   		ramble_theme_get_featured_img_with_link('ramble-theme-blog-two-grid');
                                    ?>
                                </figure> <!-- /.post-thumb --> 
                            </div> <!-- /.col-md-5 -->
                            <div class="col-md-7 col-sm-7 <?php if ($img_position == 'right') echo 'col-md-pull-5 col-sm-pull-5'; ?>">
                                <header class="entry-header">
                                    <div class="cat-links">
                                        <?php the_category(' '); ?>
                                    </div>
                                    <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                                    <div class="entry-meta">
                                        <div class="meta-content">            
                                            <div class="meta">          
                                                <span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
                                                <span class="devider">/</span>
                                                <span class="byline">
                                                    <span class="author vcard">
                                                        <?php esc_html_e('By: ','ramble').the_author_posts_link(); ?>
                                                    </span>
                                                </span>
                                                <span class="devider">/</span>
                                                <span  class="comments-link"><a href="<?php comments_link(); ?>">
	                                        	<?php comments_number( esc_html__( 'No Comments', 'ramble' ), esc_html__( '1 Comment', 'ramble' ), '%'.esc_html__( 'Comments', 'ramble' ) ); ?>
		                                    </a>
		                                    </span>
                                            </div> <!-- /.meta -->
                                        </div>
                                    </div> <!-- .entry-meta -->
                                </header> <!-- /.entry-header -->
                                <div class="entry-content">
                                    <p><?php echo ramble_theme_mag_excerpt_length($excerpt_length); ?></p>
                                </div> <!-- .entry-content -->
                            </div>  <!-- /.col-md-7 -->
                        </div> <!-- /.row -->
                    </div> <!-- /.post -->
                    <?php
                    endwhile;
                    endif;
                    ?>

                </div> <!-- /.category-content -->
            </div> <!-- /.tab-content -->
			<?php } ?>
        </div> <!-- /.tab -->
    </div> <!-- /#category-style-one -->
    <script>
    	(function($) {
	    	$("#category-tab-<?php echo $tab_shortcode_id; ?> .category-heading .right-section").each(function() {
        	    $(this).find('li:first').addClass("current");
        	    $(this).parents().next('.tab').find('.tab-container:first').show();
        	});
        	$("#category-tab-<?php echo $tab_shortcode_id; ?> .category-heading .right-section li a").on('click',function() {
        	    var categoryTab = $(this).closest('li');
        	    categoryTab.siblings('li').removeClass("current");
        	    categoryTab.addClass("current");
        	    categoryTab.closest('#category-tab-<?php echo $tab_shortcode_id; ?> .category-heading .right-section').parents().nextAll('.tab:first').find('.tab-container').hide(); 

        	    var activeTab = $(this).attr("href"); 
        	    $(activeTab).fadeIn(); 
        	    return false;
        	});
	    })(jQuery);
    </script>
	<?php	
	$tab_shortcode_id++;
	$category_block_style_tab_two = ob_get_clean();
	return $category_block_style_tab_two;
}); // end category_block_style_tab_two

// category_block_style_tab_three
add_shortcode("category_block_style_tab_three", function($atts, $content = null) {
	STATIC $tab_shortcode_id = 10;
	ob_start();	
	$atts = shortcode_atts(
		array(
			'category_slug' => '',
			'tab_title' => '',
			'total_post' => 4,
			'excerpt_length' => 350,
			'tab_position' => 'right',
			'category_column' => '2',
			'post_layout_style' => 'grid',
		), $atts
	);
	extract($atts);
	?>

	<div id="category-tab-<?php echo $tab_shortcode_id; ?>" class="all-latest-post list col-md-12">                                
        <div class="category-heading">
            <div class="left-section <?php if ($tab_position == 'left') echo 'pull-right'; ?>">
            	<?php
            	if ( !empty($tab_title) ) { 
            		echo "<h4>$tab_title</h4>";
            	} else {
            		echo "<h4>".esc_html__('Latest Post', 'ramble')."</h4>";
            	}
            	?> 
            </div> <!-- /.left-section -->
            <div class="right-section <?php if ($tab_position == 'left') echo 'pull-left'; ?>">
                <ul>
                	<?php
                		$category_slug = str_replace(" ","", $category_slug);
                		$category_slugs = explode(",", $category_slug);
                		$tab_li_i = 1;
                		if ( !empty($category_slug) ) {
		                	echo "<li class='current'><a href='#$tab_shortcode_id-tab-0'>".__('All', 'ramble')."</a></li>";
		                }
                		foreach ($category_slugs as $key => $value) {
                			if ( !empty($value) ) {
                				$cat_name = get_category_by_slug($value); 
	                			echo "<li><a href='#$tab_shortcode_id-tab-$tab_li_i'>".$cat_name->name."</a></li>";
                				$tab_li_i++;
                			}
                		}
                	?>	
                </ul>
            </div> <!-- /.right-section -->
        </div> <!-- /.category-heading -->
        <div class="tab">
        	<?php 
    		for ($x = 0; $x < $tab_li_i; $x++) {
    			$cat_slugs = array_filter($category_slugs);
    			if ($x == 0) {
	    			$cat_slug = $cat_slugs;
    			} else {
    				$cat_slug = $cat_slugs[$x-1];
    			}
			?>
            <div id="<?php echo $tab_shortcode_id; ?>-tab-<?php echo $x; ?>" class="tab-container">                                            
                <div class="category-content">
        	    	<?php 
        	    	$total_posts = $total_post;
        	    	if ( !empty($category_slug) ) {
	        	    	$post_by_cat_condition = array(
        		    	        array(
        		    	            'taxonomy' => 'category',
        		    	            'field' => 'slug',
        		    	            'terms' => $cat_slug 
        		    	        )
        		    	    );
        	    	} else {
        	    		$post_by_cat_condition = '';
        	    	}
        	    	$wp_query = new WP_Query(
        	    	    array (
        	    	    	'tax_query' => $post_by_cat_condition,
        	    	        'posts_per_page' => $total_posts,
        	    	        'ignore_sticky_posts' => true,
        	    	    )
        	    	);
        	    	
        	    	if ( $wp_query->have_posts() ) : 
        	    	$ramble_theme_post_i = 1;      
        	    	$max = $wp_query->post_count;

        	    	if ( $category_column == "2" ) {
        	    	    $post_in_row = 2;
        	    	    $column_grid_class = "col-md-6";
        	    	} elseif ( $category_column == "3" ) {
        	    	    $post_in_row = 3;
        	    	    $column_grid_class = "col-md-4";
        	    	} elseif ( $category_column == "4" ) {
        	    	    $post_in_row = 4;
        	    	    $column_grid_class = "col-md-3";
        	    	}

        	    	// show start row in masonry on
        	    	echo ( $post_layout_style == 'masonry' ) ? "<div class='row masonry'>" : "";

        	    	while ( $wp_query->have_posts() ) : $wp_query->the_post(); 

        	    	if ( $post_layout_style != 'masonry' ) {
        	    	    if ( $ramble_theme_post_i % $post_in_row == 1 && $post_in_row != 1 ) {
        	    	        echo "<div class='row'>";
        	    	    }  
        	    	}           
        	    	?>
                    <div class="<?php echo $column_grid_class; ?> col-sm-6">
                        <div class="post">
                            <figure class="post-thumb">
                                <?php
                                	ramble_theme_get_featured_img_with_link('ramble-theme-blog-two-grid');
                                ?>
                            </figure> <!-- /.post-thumb --> 
                            <header class="entry-header">
                                <div class="cat-links">
                                    <?php the_category(' '); ?>
                                </div>
                                <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                                <div class="entry-meta">
                                    <div class="meta-content">            
                                        <div class="meta">          
                                            <span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
                                            <span class="devider">/</span>
                                            <span class="byline">
                                                <span class="author vcard">
                                                    <?php esc_html_e('By: ','ramble').the_author_posts_link(); ?>
                                                </span>
                                            </span>
                                            <span class="devider">/</span>
                                            <span  class="comments-link"><a href="<?php comments_link(); ?>">
                                        	<?php comments_number( esc_html__( 'No Comments', 'ramble' ), esc_html__( '1 Comment', 'ramble' ), '%'.esc_html__( 'Comments', 'ramble' ) ); ?>
	                                    </a>
	                                    </span>
                                        </div> <!-- /.meta -->
                                    </div>
                                </div> <!-- .entry-meta -->
                            </header> <!-- /.entry-header -->
                            <div class="entry-content">
                                <p><?php echo ramble_theme_mag_excerpt_length($excerpt_length); ?></p>
                            </div> <!-- .entry-content -->
                        </div> <!-- /.post -->
                    </div> <!-- /.col-md-6 -->
                    <?php
                    if ( $post_layout_style != 'masonry' ) {
                        if ( $ramble_theme_post_i % $post_in_row == 0 && $post_in_row !=1 ) {
                            echo "</div><!-- /.row-->";
                        } elseif( $ramble_theme_post_i == $max && $max % $post_in_row != 0 && $post_in_row != 1 ) {
                            echo "</div><!-- /.row-->";
                        }
                        $ramble_theme_post_i++;
                    }
                    ?>
                    <?php
                    endwhile; 
                    // show end row in masonry on
                    echo ( $post_layout_style == 'masonry' ) ? "</div>" : "";
                    ?>
                    <?php endif; ?>

                </div> <!-- /.category-content -->
            </div> <!-- /.tab-content -->
			<?php } ?>
        </div> <!-- /.tab -->
    </div> <!-- /#category-style-one -->
    <script>
    	(function($) {
	    	$("#category-tab-<?php echo $tab_shortcode_id; ?> .category-heading .right-section").each(function() {
        	    $(this).find('li:first').addClass("current");
        	    $(this).parents().next('.tab').find('.tab-container:first').show();
        	});
        	$("#category-tab-<?php echo $tab_shortcode_id; ?> .category-heading .right-section li a").on('click',function() {
        	    var categoryTab = $(this).closest('li');
        	    categoryTab.siblings('li').removeClass("current");
        	    categoryTab.addClass("current");
        	    categoryTab.closest('#category-tab-<?php echo $tab_shortcode_id; ?> .category-heading .right-section').parents().nextAll('.tab:first').find('.tab-container').hide(); 

        	    var activeTab = $(this).attr("href"); 
        	    $(activeTab).fadeIn(); 
        	    return false;
        	});
	    })(jQuery);
    </script>
	<?php	
	$tab_shortcode_id++;
	$category_block_style_tab_three = ob_get_clean();
	return $category_block_style_tab_three;
}); // end category_block_style_tab_three

/* Mega Menu Shortcode */
// menu_category_column_post
add_shortcode("menu_category_column_post", function($atts, $content = null) {
	ob_start();	
	$atts = shortcode_atts(
		array(
			'category_slug' => '',
			'total_post' => 4,
			'category_column' => '4',
		), $atts
	);
	extract($atts);
	?>

	<div class="megacat cat_full">
        <div class="megamenu-content col-md-12">
            <div class="cat-wrap">
	    	<?php 
	    	$cat_slug = get_category_by_slug($category_slug); 
            $cat_id = $cat_slug->term_id;
	    	if ( !empty($category_slug) ) {
    	    	$post_by_cat_condition = array(
		    	        array(
		    	            'taxonomy' => 'category',
		    	            'field' => 'slug',
		    	            'terms' => $category_slug 
		    	        )
		    	    );
	    	} else {
	    		$post_by_cat_condition = '';
	    	}
	    	$wp_query = new WP_Query(
	    	    array (
	    	    	'tax_query' => $post_by_cat_condition,
	    	        'posts_per_page' => $total_post,
	    	        'ignore_sticky_posts' => true,
	    	    )
	    	);
	    	
	    	if ( $wp_query->have_posts() ) : 
	    	$ramble_theme_post_i = 1;      
	    	$max = $wp_query->post_count;

	    	if ( $category_column == "2" ) {
	    	    $post_in_row = 2;
	    	    $column_grid_class = "col-md-6";
	    	} elseif ( $category_column == "3" ) {
	    	    $post_in_row = 3;
	    	    $column_grid_class = "col-md-4";
	    	} elseif ( $category_column == "4" ) {
	    	    $post_in_row = 4;
	    	    $column_grid_class = "col-md-3";
	    	}

	    	while ( $wp_query->have_posts() ) : $wp_query->the_post(); 

    	    if ( $ramble_theme_post_i % $post_in_row == 1 && $post_in_row != 1 ) {
    	        echo "<ul class='row'>";
    	    }           
	    	?>
            <li class="<?php echo $column_grid_class;  ?>">
                <div class="post-thumb bg-image">
                    <?php
                   		ramble_theme_get_featured_img_with_link('ramble-theme-blog-two-grid');
                    ?>
                </div>
                <div class="post-content">
                    <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
                    <span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
                </div>
            </li>
            <?php
            if ( $ramble_theme_post_i % $post_in_row == 0 && $post_in_row !=1 ) {
                echo "</ul><!-- /.row-->";
            } elseif( $ramble_theme_post_i == $max && $max % $post_in_row != 0 && $post_in_row != 1 ) {
                echo "</ul><!-- /.row-->";
            }
            $ramble_theme_post_i++;
            ?>
            <?php
            endwhile; 
            ?>
            <?php endif; ?>
                <div class="view-all">
                    <a href="<?php echo esc_url( get_category_link( $cat_id ) ); ?>"><?php esc_html_e( 'View All', 'ramble' ); ?> <i class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>                                                        
        </div>
    </div>
	<?php	
	$menu_category_column_post = ob_get_clean();
	return $menu_category_column_post;
}); // end menu_category_column_post


/* Portfolio Shortcode */
// recent_portfolio_item
add_shortcode("recent_portfolio_item", function($atts, $content = null) {
	ob_start();	
	$atts = shortcode_atts(
		array(
			'total_post' => 4,
			'portfolio_title' => esc_html__('Our Latest Portfolio Item', 'ramble'),
		), $atts
	);
	extract($atts);
	?>
	<div class="recent-portfolio-item"> 
	    <div class="item-header">
	        <div class="pull-left">
	            <h4><?php echo esc_html($portfolio_title); ?></h4>
	        </div>
	        <div class="pull-right">
	            <div class="control-latest-carousel">
	                <span class="previous">
	                    <i class="fa fa-angle-left"></i>
	                </span>
	                <span class="next">
	                    <i class="fa fa-angle-right"></i>
	                </span>
	            </div>
	        </div>
	    </div>
	    <div class="clearfix"></div>
	    <div class="portfolio">    	
		    <div class="new-products owl-carousel">
		    	<?php
		    	global $post;
		    	if ( isset( $post->ID ) ) { $meta = get_post_meta( $post->ID ); }
			    $query = new WP_Query ( array ( 
			            'post_type' => 'portfolio',
			            'posts_per_page' => $total_post
			        )
			    );
			    if ( $query->have_posts() ) : 
			    while ( $query->have_posts() ) : $query->the_post(); 
		    	?>
		        <div class="item">                              
		            <div class="col-md-12">
		    			<div class="post-portfolio">
		    			    <figure class="post-thumb">
	                            <?php  
                                    if ( wp_get_attachment_image( get_post_meta( get_the_ID(), '_ramble_theme_portfolio_thumbnail_id', 1 ), 'ramble-theme-portfolio-image' ) ) {  ?>
                                        <a href="<?php the_permalink(); ?>">
                                        <?php
                                        echo wp_get_attachment_image( get_post_meta( get_the_ID(), '_ramble_theme_portfolio_thumbnail_id', 1 ), 'ramble-theme-portfolio-image' );
                                        ?>
                                        </a> 
                                        <?php
                                    } else { ?>
                                        <a href="<?php the_permalink(); ?>">
                                            <img src="<?php echo get_template_directory_uri().'/images/nopreview.png'; ?>" alt="<?php the_title(); ?>">
                                        </a>                            
                                        <?php 
                                    }
                                ?>
		    			    </figure> <!-- /.post-thumb -->
		    			    <div class="entry-header">
		    			    	<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
		    			    	<div class="entry-meta">
		    			    	    <span class="cat-links">
		    			    	        <?php 
		    			    	        $category = get_the_terms( $post->ID, 'portfolio' );     
		    			    	        foreach ( $category as $cat ) {
		    			    	           echo $cat->name.', ';
		    			    	        }
		    			    	        ?>
		    			    	    </span>
		    			    	</div> <!-- .entry-meta -->
		    			    </div> <!-- /.entry-header -->
		    			</div> <!-- /.post --> 
		            </div> <!-- /.col-md-12 -->
		        </div> <!-- /.item -->
		        <?php endwhile; endif; ?>
		    </div> <!-- /#new-products -->
	    </div> <!-- /.portfolio -->                          
	</div> <!-- /.portfolio-item -->	
	<?php	
	$recent_portfolio_item = ob_get_clean();
	return $recent_portfolio_item;
}); // end recent_portfolio_item

// recent_shop_item
add_shortcode("recent_shop_item", function($atts, $content = null) {
	ob_start();	
	$atts = shortcode_atts(
		array(
			'total_post' => 4,
			'shop_item_title' => esc_html__('Our Latest Shop Item', 'ramble'),
		), $atts
	);
	extract($atts);
	?>
	<div class="shop-layout woocommerce"> 
	    <div class="item-header">
	        <div class="pull-left">
	            <h4><?php echo esc_html($shop_item_title); ?></h4>
	        </div>
	        <div class="pull-right">
	            <div class="control-latest-carousel">
	                <span class="previous">
	                    <i class="fa fa-angle-left"></i>
	                </span>
	                <span class="next">
	                    <i class="fa fa-angle-right"></i>
	                </span>
	            </div>
	        </div>
	    </div>
	    <div class="clearfix"></div>                           
	    <div class="new-products owl-carousel">
	    	<?php
	    	global $post, $product;


	    	if ( isset( $post->ID ) ) { $meta = get_post_meta( $post->ID ); }
		    $query = new WP_Query ( array ( 
		            'post_type' => 'product',
		            'posts_per_page' => $total_post
		        )
		    );
		    if ( $query->have_posts() ) : 
		    while ( $query->have_posts() ) : $query->the_post(); 
	    	?>
	        <div class="item">                              
	            <div class="col-md-12">
	                <div class="single-product">
	                    <div class="product-box">
	                        <div class="single-img">
	                        	<?php  
                                    if ( get_the_post_thumbnail() ) {  ?>
                                        <a href="<?php the_permalink(); ?>">
                                        <?php
                                        	the_post_thumbnail();
                                        ?>
                                        </a> 
                                        <?php
                                    } else { ?>
                                        <a href="<?php the_permalink(); ?>">
                                            <img src="<?php echo get_template_directory_uri().'/images/nopreview.png'; ?>" alt="<?php the_title(); ?>">
                                        </a>                            
                                        <?php 
                                    }
                                ?>
	                        </div> <!-- /.single-img -->  
	                    </div> <!-- /.product-box -->
	                    <div class="product-info">
                            <div class="info-container">             
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
                                <?php global $product; ?>

                                <?php if ( $rating_html = $product->get_rating_html() ) : ?>
                                	<div class="star-rating clearfix"><?php echo $rating_html; ?></div>
                                <?php endif; ?>

                                <div class="price clearfix">
                                    <?php if ( $price_html = $product->get_price_html() ) : ?>
                                    	<span class="price clearfix"><?php echo $price_html; ?></span>
                                    <?php endif; ?>
                                </div>
                                
                                <div class="add-cart">
                                    <?php
                                    echo apply_filters( 'woocommerce_loop_add_to_cart_link',
                                    	sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
                                    		esc_url( $product->add_to_cart_url() ),
                                    		esc_attr( isset( $quantity ) ? $quantity : 1 ),
                                    		esc_attr( $product->id ),
                                    		esc_attr( $product->get_sku() ),
                                    		esc_attr( isset( $class ) ? $class : 'button product_type_simple add_to_cart_button ajax_add_to_cart' ),
                                    		esc_html( $product->add_to_cart_text() )
                                    	),
                                    $product );
                                    ?>
                                </div>
                            </div> <!-- /.info-container -->
                        </div> <!-- /.product-info -->
	                </div> <!-- /.single-product -->  
	            </div> <!-- /.col-md-12 -->
	        </div> <!-- /.item -->
	        <?php endwhile; endif; ?>
	    </div> <!-- /#new-products -->
	</div> <!-- /.portfolio-item -->
	<?php	
	$recent_shop_item = ob_get_clean();
	return $recent_shop_item;
}); // end recent_shop_item