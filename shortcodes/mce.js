(function() {
	tinymce.PluginManager.add('ramble_mce_button', function( editor, url ) {
		editor.addButton( 'ramble_mce_button', {
            text: rambleLang.shortcode_ramble, 
            icon: false,
            tooltip: rambleLang.shortcode_ramble,
            type: 'menubutton',
			minWidth: 210,
			menu: [
				{
					text: rambleLang.shortcode_magazine,
                    menu: [
                        {
                            text: rambleLang.shortcode_cat_block_style_col,
                            onclick: function() {
                            	editor.windowManager.open( {
                            		title: rambleLang.shortcode_cat_block_style_col,
                            		body: [
                            			{
                            				type: 'listbox',
                            				name: 'category_slug',
                            				label: rambleLang.shortcode_cat,
                            				'values': rambleLang.shortcode_category_list
                            			},
                            			{
                            				type: 'textbox',
                            				name: 'total_post',
                            				label: rambleLang.shortcode_number_of_posts,
                            				minWidth: 200,
                            				value: '4'
                            			}, 
                            			{
                            				type: 'textbox',
                            				name: 'excerpt_length',
                            				label: rambleLang.shortcode_1st_post_exc_length,
                            				minWidth: 200,
                            				value: '350'
                            			},
                            			{
                            				type: 'listbox',
                            				name: 'category_column',
                            				label: rambleLang.shortcode_cat_column_size,
                            				'values': [
												{text: rambleLang.shortcode_6_grid, value: 'col-md-6'},
												{text: rambleLang.shortcode_4_grid, value: 'col-md-4'},
												{text: rambleLang.shortcode_3_grid, value: 'col-md-3'},
											]
                            			},
                            		],
                            		onsubmit: function( e ) {
                            			editor.insertContent( '[category_block_style_column category_slug="' + e.data.category_slug + '" total_post="' + e.data.total_post + '" excerpt_length="' + e.data.excerpt_length + '" category_column="' + e.data.category_column + '"]');
                            		}
                            	});
                            } // onclick
                        },
                        {
                            text: rambleLang.shortcode_cat_block_style_slider,
                            onclick: function() {
                            	editor.windowManager.open( {
                            		title: rambleLang.shortcode_cat_block_style_slider,
                            		body: [
                            			{
                            				type: 'listbox',
                            				name: 'category_slug',
                            				label: rambleLang.shortcode_cat,
                            				'values': rambleLang.shortcode_category_list
                            			},
                            			{
                            				type: 'textbox',
                            				name: 'total_post',
                            				label: rambleLang.shortcode_number_of_posts,
                            				minWidth: 200,
                            				value: '6'
                            			}, 
                            			{
                            				type: 'textbox',
                            				name: 'excerpt_length',
                            				label: rambleLang.shortcode_1st_post_exc_length,
                            				minWidth: 200,
                            				value: '350'
                            			},
                            			{
                            				type: 'listbox',
                            				name: 'show_item',
                            				label: rambleLang.shortcode_show_item,
                            				'values': [
												{text: "2", value: '2'},
												{text: "3", value: '3'},
												{text: "4", value: '4'},
											]
                            			},
                            		],
                            		onsubmit: function( e ) {
                            			editor.insertContent( '[category_block_style_slider category_slug="' + e.data.category_slug + '" total_post="' + e.data.total_post + '" excerpt_length="' + e.data.excerpt_length + '" show_item="' + e.data.show_item + '"]');
                            		}
                            	});
                            } // onclick
                        },
                        {
                            text: rambleLang.shortcode_cat_block_style_tab_1,
                            onclick: function() {
                            	editor.windowManager.open( {
                            		title: rambleLang.shortcode_cat_block_style_tab_1,
                            		body: [
                            			{
                            				type: 'textbox',
                            				name: 'category_slug',
                            				label: rambleLang.shortcode_cat_slug,
                            				'values': ''
                            			},
                            			{
                            			type: 'label',
							            name: 'category_slug_label',
							            multiline: true,
							            text: "",
							            onPostRender : function() {
							                this.getEl().innerHTML =
							                   rambleLang.shortcode_cat_slug_description;}
							            },
							            {
                            				type: 'textbox',
                            				name: 'tab_title',
                            				label: rambleLang.shortcode_tab_title,
                            				value: ''
                            			},
                            			{
                            				type: 'textbox',
                            				name: 'total_post',
                            				label: rambleLang.shortcode_number_of_posts,
                            				minWidth: 200,
                            				value: '6'
                            			}, 
                            			{
                            				type: 'textbox',
                            				name: 'excerpt_length',
                            				label: rambleLang.shortcode_1st_post_exc_length,
                            				minWidth: 200,
                            				value: '350'
                            			},
                            			{
                            				type: 'listbox',
                            				name: 'tab_position',
                            				label: rambleLang.shortcode_tab_position,
                            				'values': [
												{text: rambleLang.shortcode_right, value: 'right'},
												{text: rambleLang.shortcode_left, value: 'left'},
											]
                            			},
                            			{
                            				type: 'listbox',
                            				name: 'big_post_position',
                            				label: rambleLang.shortcode_big_post_position,
                            				'values': [
												{text: rambleLang.shortcode_left, value: 'left'},
												{text: rambleLang.shortcode_right, value: 'right'},
											]
                            			},
                            		],
                            		onsubmit: function( e ) {
                            			editor.insertContent( '[category_block_style_tab_one category_slug="' + e.data.category_slug + '" tab_title="' + e.data.tab_title + '" total_post="' + e.data.total_post + '" excerpt_length="' + e.data.excerpt_length + '" tab_position="' + e.data.tab_position + '" big_post_position="' + e.data.big_post_position + '"]');
                            		}
                            	});
                            } // onclick
                        },
                        {
                            text: rambleLang.shortcode_cat_block_style_tab_2,
                            onclick: function() {
                            	editor.windowManager.open( {
                            		title: rambleLang.shortcode_cat_block_style_tab_2,
                            		body: [
                            			{
                            				type: 'textbox',
                            				name: 'category_slug',
                            				label: rambleLang.shortcode_cat_slug,
                            				'values': ''
                            			},
                            			{
                            			type: 'label',
							            name: 'category_slug_label',
							            multiline: true,
							            text: "",
							            onPostRender : function() {
							                this.getEl().innerHTML =
							                   rambleLang.shortcode_cat_slug_description;}
							            },
							            {
                            				type: 'textbox',
                            				name: 'tab_title',
                            				label: rambleLang.shortcode_cat,
                            				value: ''
                            			},
                            			{
                            				type: 'textbox',
                            				name: 'total_post',
                            				label: rambleLang.shortcode_number_of_posts,
                            				minWidth: 200,
                            				value: '4'
                            			}, 
                            			{
                            				type: 'textbox',
                            				name: 'excerpt_length',
                            				label: rambleLang.shortcode_1st_post_exc_length,
                            				minWidth: 200,
                            				value: '350'
                            			},
                            			{
                            				type: 'listbox',
                            				name: 'tab_position',
                            				label: rambleLang.shortcode_tab_position,
                            				'values': [
												{text: rambleLang.shortcode_right, value: 'right'},
												{text: rambleLang.shortcode_left, value: 'left'},
											]
                            			},
                            			{
                            				type: 'listbox',
                            				name: 'img_position',
                            				label: rambleLang.shortcode_img_position,
                            				'values': [
												{text: rambleLang.shortcode_left, value: 'left'},
												{text: rambleLang.shortcode_right, value: 'right'},
											]
                            			},
                            		],
                            		onsubmit: function( e ) {
                            			editor.insertContent( '[category_block_style_tab_two category_slug="' + e.data.category_slug + '" tab_title="' + e.data.tab_title + '" total_post="' + e.data.total_post + '" excerpt_length="' + e.data.excerpt_length + '" tab_position="' + e.data.tab_position + '" img_position="' + e.data.img_position + '"]');
                            		}
                            	});
                            } // onclick
                        },
                        {
                            text: rambleLang.shortcode_cat_block_style_tab_3,
                            onclick: function() {
                            	editor.windowManager.open( {
                            		title: rambleLang.shortcode_cat_block_style_tab_3,
                            		body: [
                            			{
                            				type: 'textbox',
                            				name: 'category_slug',
                            				label: rambleLang.shortcode_cat_slug,
                            				'values': ''
                            			},
                            			{
                            			type: 'label',
							            name: 'category_slug_label',
							            multiline: true,
							            text: "",
							            onPostRender : function() {
							                this.getEl().innerHTML =
							                   rambleLang.shortcode_cat_slug_description;}
							            },
							            {
                            				type: 'textbox',
                            				name: 'tab_title',
                            				label: rambleLang.shortcode_tab_title,
                            				value: ''
                            			},
                            			{
                            				type: 'textbox',
                            				name: 'total_post',
                            				label: rambleLang.shortcode_number_of_posts,
                            				minWidth: 200,
                            				value: '4'
                            			}, 
                            			{
                            				type: 'textbox',
                            				name: 'excerpt_length',
                            				label: rambleLang.shortcode_1st_post_exc_length,
                            				minWidth: 200,
                            				value: '350'
                            			},
                            			{
                            				type: 'listbox',
                            				name: 'tab_position',
                            				label: rambleLang.shortcode_tab_position,
                            				'values': [
												{text: rambleLang.shortcode_right, value: 'right'},
												{text: rambleLang.shortcode_left, value: 'left'},
											]
                            			},
                            			{
                            				type: 'listbox',
                            				name: 'category_column',
                            				label: rambleLang.shortcode_cat_column,
                            				'values': [
												{text: "2", value: '2'},
												{text: "3", value: '3'},
												{text: "4", value: '4'},
											]
                            			},
                            			{
                            				type: 'listbox',
                            				name: 'post_layout_style',
                            				label: rambleLang.shortcode_post_layout_style,
                            				'values': [
												{text: rambleLang.shortcode_grid, value: 'grid'},
												{text: rambleLang.shortcode_masonry, value: 'masonry'},
											]
                            			},
                            		],
                            		onsubmit: function( e ) {
                            			editor.insertContent( '[category_block_style_tab_three category_slug="' + e.data.category_slug + '" tab_title="' + e.data.tab_title + '" total_post="' + e.data.total_post + '" excerpt_length="' + e.data.excerpt_length + '" tab_position="' + e.data.tab_position + '" category_column="' + e.data.category_column + '" post_layout_style="' + e.data.post_layout_style + '"]');
                            		}
                            	});
                            } // onclick
                        },
 
                    ]
				},
				{
					text: rambleLang.shortcode_button,
					onclick: function() {
						editor.windowManager.open( {
							title: rambleLang.shortcode_button,
							body: [
								{
									type: 'listbox',
									name: 'ButtonType',
									label: rambleLang.shortcode_type,
									'values': [
										{text: rambleLang.shortcode_default, value: 'btn-default'},
										{text: rambleLang.shortcode_primary, value: 'btn-primary'},
										{text: rambleLang.shortcode_success, value: 'btn-success'},
										{text: rambleLang.shortcode_info, value: 'btn-info'},
										{text: rambleLang.shortcode_warning, value: 'btn-warning'},
										{text: rambleLang.shortcode_danger, value: 'btn-danger'},
									]
								},
								{
									type: 'listbox',
									name: 'ButtonSize',
									label: rambleLang.shortcode_size,
									'values': [
										{text: rambleLang.shortcode_size_large, value: 'btn-lg'},
										{text: rambleLang.shortcode_size_default, value: ''},
										{text: rambleLang.shortcode_size_small, value: 'btn-sm'},
										{text: rambleLang.shortcode_size_ex_small, value: 'btn-xs'},
									]
								},
								{
									type: 'textbox',
									name: 'ButtonText',
									label: rambleLang.shortcode_text,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[button type="' + e.data.ButtonType + '" size="' + e.data.ButtonSize + '" text="' + e.data.ButtonText + '"]');
							}
						});
					} // onclick
				},
				{
					text: rambleLang.shortcode_progressbar,
					onclick: function() {
						editor.windowManager.open( {
							title: rambleLang.shortcode_progressbar,
							body: [
								{
									type: 'listbox',
									name: 'ProgressbarType',
									label: rambleLang.shortcode_type,
									'values': [
										{text: rambleLang.shortcode_default, value: 'default'},
										{text: rambleLang.shortcode_primary, value: 'primary'},
										{text: rambleLang.shortcode_success, value: 'success'},
										{text: rambleLang.shortcode_info, value: 'info'},
										{text: rambleLang.shortcode_warning, value: 'warning'},
										{text: rambleLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'textbox',
									name: 'ProgressbarWidth',
									label: rambleLang.shortcode_width,
									minWidth: 300,
									value: '50'
								},
								{
									type: 'textbox',
									name: 'ProgressbarTitle',
									label: rambleLang.shortcode_title,
									minWidth: 300,
									value: ''
								},
								{
									type: 'listbox',
									name: 'ProgressbarStriped',
									label: rambleLang.shortcode_striped,
									'values': [
										{text: rambleLang.shortcode_true, value: 'true'},
										{text: rambleLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'listbox',
									name: 'ProgressbarAnimation',
									label: rambleLang.shortcode_animation,
									'values': [
										{text: rambleLang.shortcode_true, value: 'true'},
										{text: rambleLang.shortcode_false, value: 'false'},
									]
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[progress_bar type="' + e.data.ProgressbarType + '" width="' + e.data.ProgressbarWidth + '" title="' + e.data.ProgressbarTitle + '" striped="' + e.data.ProgressbarStriped + '" animation="' + e.data.ProgressbarAnimation + '"]');
							}
						});
					}
				},
				{
					text: rambleLang.shortcode_status,
					onclick: function() {
						editor.windowManager.open( {
							title: rambleLang.shortcode_status,
							body: [
								{
									type: 'listbox',
									name: 'StatusType',
									label: rambleLang.shortcode_type,
									'values': [
										{text: rambleLang.shortcode_facebook, value: 'facebook'},
										{text: rambleLang.shortcode_tweeter, value: 'tweeter'},
										{text: rambleLang.shortcode_google_plus, value: 'google_plus'},
									]
								},
								{
									type: 'textbox',
									name: 'StatusURL',
									label: rambleLang.shortcode_url,
									minWidth: 300,
									value: ''
								},
								{
									type: 'textbox',
									name: 'StatusBackground',
									label: rambleLang.shortcode_background_img,
									minWidth: 300,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[status type="' + e.data.StatusType + '" url="' + e.data.StatusURL + '" background="' + e.data.StatusBackground + '"]');
							}
						});
					}
				},
				{
					text: rambleLang.shortcode_alert,
					onclick: function() {
						editor.windowManager.open( {
							title: rambleLang.shortcode_alert,
							body: [
								{
									type: 'listbox',
									name: 'AlertType',
									label: rambleLang.shortcode_type,
									'values': [
										{text: rambleLang.shortcode_default, value: 'default'},
										{text: rambleLang.shortcode_primary, value: 'primary'},
										{text: rambleLang.shortcode_success, value: 'success'},
										{text: rambleLang.shortcode_info, value: 'info'},
										{text: rambleLang.shortcode_warning, value: 'warning'},
										{text: rambleLang.shortcode_danger, value: 'danger'},
									]
								},
								{
									type: 'listbox',
									name: 'AlertDismiss',
									label: rambleLang.shortcode_dismiss,
									'values': [
										{text: rambleLang.shortcode_true, value: 'true'},
										{text: rambleLang.shortcode_false, value: 'false'},
									]
								},
								{
									type: 'textbox',
									name: 'AlertContent',
									label: rambleLang.shortcode_content,
									value: '',									
									multiline: true,
									minWidth: 300,
									minHeight: 100
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[alert type="' + e.data.AlertType + '" dismiss="' + e.data.AlertDismiss + '"]' + e.data.AlertContent + '[/alert]');
							}
						});
					}
				},
				{
					text: rambleLang.shortcode_video,
					onclick: function() {
						editor.windowManager.open( {
							title: rambleLang.shortcode_video,
							body: [
								{
									type: 'textbox',
									name: 'VideoURL',
									label: rambleLang.shortcode_video_url,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'VideoWidth',
									label: rambleLang.shortcode_width,
									value: ''
								},
								{
									type: 'textbox',
									name: 'Videoheight',
									label: rambleLang.shortcode_height,
									value: ''
								},
							],
							onsubmit: function( e ) {
								editor.insertContent( '[embed width="' + e.data.VideoWidth + '" height="' + e.data.Videoheight + '"]' + e.data.VideoURL + '[/embed]');
							}
						});
					}
				},
				{
					text: rambleLang.shortcode_audio,
					onclick: function() {
						editor.windowManager.open( {
							title: rambleLang.shortcode_audio,
							body: [
								{
									type: 'textbox',
									name: 'mp3URL',
									label: rambleLang.shortcode_mp3,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'm4aURL',
									label: rambleLang.shortcode_m4a,
									value: 'http://',
									minWidth: 300,
								},
								{
									type: 'textbox',
									name: 'oggURL',
									label: rambleLang.shortcode_ogg,
									value: 'http://',
									minWidth: 300,
								},
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[audio mp3="' + e.data.mp3URL + '" m4a="' + e.data.m4aURL + '" ogg="' + e.data.oggURL + '"]');
							}
						});
					}
				},
				{
					text: rambleLang.shortcode_tooltip,
					onclick: function() {
						editor.windowManager.open( {
							title: rambleLang.shortcode_tooltip,
							body: [
								{
									type: 'textbox',
									name: 'TooltipText',
									label: rambleLang.shortcode_text,
									value: '',
									minWidth: 300,
								},
								{
									type: 'listbox',
									name: 'TooltipDirection',
									label: rambleLang.shortcode_direction,
									'values': [
										{text: rambleLang.shortcode_top, value: 'top'},
										{text: rambleLang.shortcode_right, value: 'right'},
										{text: rambleLang.shortcode_bottom, value: 'bottom'},
										{text: rambleLang.shortcode_left, value: 'left'},
									]
								},
								{
									type: 'textbox',
									name: 'ToolTipContent',
									label: rambleLang.shortcode_content,
									value: '',
									multiline: true,
									minWidth: 300,
									minHeight: 100
								}
								
							],
							onsubmit: function( e ) {
								editor.insertContent( '[tooltip text="' + e.data.TooltipText + '" direction="' + e.data.TooltipDirection + '"]'+ e.data.ToolTipContent +'[/tooltip]');
							}
						});
					}
				},
				{
					text: rambleLang.shortcode_columns,
						onclick: function() {
							editor.insertContent( '\
							[row]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
								[column number="6" offset=""] Column Content Goes Here [/column]<br />\
							[/row]');
						}
				},
				{
					text: rambleLang.shortcode_accordion,
						onclick: function() {
							editor.insertContent( '\
							[collapse_group]<br />\
								[collapse id="accordion_one" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
								[collapse id="accordion_two" title="Accordion Title" expanded="false"] Accordion Content [/collapse]<br />\
							[/collapse_group]');
						}
				},
				{
					text: rambleLang.shortcode_tab,
						onclick: function() {
							editor.insertContent( '\
							[tabs]<br />\
								[tab id="tab_one" title="Tab Title" active="active"] Tab Content [/tab]<br />\
								[tab id="tab_two" title="Tab Title"] Tab Content [/tab]<br />\
								[tab id="tab_three" title="Tab Title"] Tab Content [/tab]<br />\
							[/tabs]');
						}
				},
                {
                    text: rambleLang.shortcode_custom_icon,
                    classes: 'sh-custom-icon',
                },
			]
		});
	});
})();